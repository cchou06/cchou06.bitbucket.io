var namespaces_dup =
[
    [ "BN0055", null, [
      [ "BN0055", "classBN0055_1_1BN0055.html", "classBN0055_1_1BN0055" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop1", null, [
      [ "ClosedLoop", "classClosedLoop1_1_1ClosedLoop.html", "classClosedLoop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV88471", null, [
      [ "DRV8847", "classDRV88471_1_1DRV8847.html", "classDRV88471_1_1DRV8847" ],
      [ "Motor", "classDRV88471_1_1Motor.html", "classDRV88471_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01", "namespaceHW__0x01.html", [
      [ "getChange", "namespaceHW__0x01.html#abfa185fb49bdf353e198ad02ab389654", null ],
      [ "cng", "namespaceHW__0x01.html#a868cbf54029436fb6e6d4b6aa8146051", null ],
      [ "cngVal", "namespaceHW__0x01.html#a4740893c3a074a5bd64df64e0d401f45", null ],
      [ "denoms", "namespaceHW__0x01.html#a133ac2b808698b3316db8c9211667639", null ],
      [ "pmt", "namespaceHW__0x01.html#a4dbb0a8dbed6fc8408ff541a2b051e8e", null ],
      [ "pmtVal", "namespaceHW__0x01.html#ad638f997e0a837494e540d388d0276ab", null ],
      [ "price", "namespaceHW__0x01.html#a6ffa02ed681f39b50cd02eb097c3f254", null ],
      [ "test_cases", "namespaceHW__0x01.html#a73f90f8633cce5f280ebeb6c31b8abf8", null ]
    ] ],
    [ "lab0x01", null, [
      [ "runs", "lab0x01_8py.html#a34d7317b45ea231655c0ef422c243657", null ],
      [ "state", "lab0x01_8py.html#a27fb060f3747fdf8919adb8b2edda416", null ]
    ] ],
    [ "lab2main", null, [
      [ "main", "lab2main_8py.html#af77336d67582e8c6cae8a642dff48231", null ]
    ] ],
    [ "lab3main", null, [
      [ "main", "lab3main_8py.html#a8a674d6d4c43f77d20f9fca1dfdaee3b", null ]
    ] ],
    [ "lab4main", null, [
      [ "main", "lab4main_8py.html#a934f1ae0aebdfa8b805c5dd033032d41", null ]
    ] ],
    [ "labfinal", null, [
      [ "main", "labfinal_8py.html#ab1cf36a10c5c065bc0d6c04439498eb4", null ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_controller1", null, [
      [ "Task_Controller", "classtask__controller1_1_1Task__Controller.html", "classtask__controller1_1_1Task__Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_IMU1", null, [
      [ "Task_IMU", "classtask__IMU1_1_1Task__IMU.html", "classtask__IMU1_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1", null, [
      [ "Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user1", null, [
      [ "Task_User", "classtask__user1_1_1Task__User.html", "classtask__user1_1_1Task__User" ]
    ] ],
    [ "touchscreen1", null, [
      [ "Touchscreen", "classtouchscreen1_1_1Touchscreen.html", "classtouchscreen1_1_1Touchscreen" ]
    ] ]
];