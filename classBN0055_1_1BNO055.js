var classBN0055_1_1BNO055 =
[
    [ "__init__", "classBN0055_1_1BNO055.html#a492691607e0dd8d5f4a4378a248eeb5d", null ],
    [ "ang_vel", "classBN0055_1_1BNO055.html#aa64cba7874ed325f2078bc8ea853348b", null ],
    [ "calibration_coef", "classBN0055_1_1BNO055.html#a5e1629e006c588815385fdaf26cd2377", null ],
    [ "calibration_coef_write", "classBN0055_1_1BNO055.html#aae768a4a666e329c45283594f1308ebd", null ],
    [ "calibration_status", "classBN0055_1_1BNO055.html#a7e184c245dd6f48e4e3df5ca15bcbc9f", null ],
    [ "convert_heci_to_interger", "classBN0055_1_1BNO055.html#a8196bf2baa95fa7a53d030cf5783ed2f", null ],
    [ "euler", "classBN0055_1_1BNO055.html#a13bf1cc27f5e65ccf6cbfb65448133f8", null ],
    [ "mode", "classBN0055_1_1BNO055.html#a659983f6941bbd6046cb05c787f60b6d", null ]
];