var annotated_dup =
[
    [ "BN0055", null, [
      [ "BN0055", "classBN0055_1_1BN0055.html", "classBN0055_1_1BN0055" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop1", null, [
      [ "ClosedLoop", "classClosedLoop1_1_1ClosedLoop.html", "classClosedLoop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV88471", null, [
      [ "DRV8847", "classDRV88471_1_1DRV8847.html", "classDRV88471_1_1DRV8847" ],
      [ "Motor", "classDRV88471_1_1Motor.html", "classDRV88471_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor", null, [
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_controller1", null, [
      [ "Task_Controller", "classtask__controller1_1_1Task__Controller.html", "classtask__controller1_1_1Task__Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_IMU1", null, [
      [ "Task_IMU", "classtask__IMU1_1_1Task__IMU.html", "classtask__IMU1_1_1Task__IMU" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1", null, [
      [ "Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user1", null, [
      [ "Task_User", "classtask__user1_1_1Task__User.html", "classtask__user1_1_1Task__User" ]
    ] ],
    [ "touchscreen1", null, [
      [ "Touchscreen", "classtouchscreen1_1_1Touchscreen.html", "classtouchscreen1_1_1Touchscreen" ]
    ] ]
];