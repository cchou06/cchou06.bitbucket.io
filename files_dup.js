var files_dup =
[
    [ "BN0055.py", "BN0055_8py.html", [
      [ "BN0055.BN0055", "classBN0055_1_1BN0055.html", "classBN0055_1_1BN0055" ]
    ] ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop.ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop1.py", "ClosedLoop1_8py.html", [
      [ "ClosedLoop1.ClosedLoop", "classClosedLoop1_1_1ClosedLoop.html", "classClosedLoop1_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "DRV88471.py", "DRV88471_8py.html", [
      [ "DRV88471.DRV8847", "classDRV88471_1_1DRV8847.html", "classDRV88471_1_1DRV8847" ],
      [ "DRV88471.Motor", "classDRV88471_1_1Motor.html", "classDRV88471_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "HW_0x01.py", "HW__0x01_8py.html", "HW__0x01_8py" ],
    [ "lab0x01.py", "lab0x01_8py.html", "lab0x01_8py" ],
    [ "lab2main.py", "lab2main_8py.html", "lab2main_8py" ],
    [ "lab3main.py", "lab3main_8py.html", "lab3main_8py" ],
    [ "lab4main.py", "lab4main_8py.html", "lab4main_8py" ],
    [ "labfinal.py", "labfinal_8py.html", "labfinal_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_controller1.py", "task__controller1_8py.html", [
      [ "task_controller1.Task_Controller", "classtask__controller1_1_1Task__Controller.html", "classtask__controller1_1_1Task__Controller" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_IMU1.py", "task__IMU1_8py.html", [
      [ "task_IMU1.Task_IMU", "classtask__IMU1_1_1Task__IMU.html", "classtask__IMU1_1_1Task__IMU" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_motor1.py", "task__motor1_8py.html", [
      [ "task_motor1.Task_Motor", "classtask__motor1_1_1Task__Motor.html", "classtask__motor1_1_1Task__Motor" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "task_user1.py", "task__user1_8py.html", "task__user1_8py" ],
    [ "touchscreen1.py", "touchscreen1_8py.html", "touchscreen1_8py" ]
];